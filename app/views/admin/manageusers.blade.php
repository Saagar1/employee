@extends('admin.masteradmin')

@section('title')
@parent
<title>Manage Users</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-users sidebar-nav-icon"></i>Manage Users<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li>Manageusers</li>
    </ul>
    <!-- END Blank Header -->

    <!-- Example Block -->
    <div class="row">
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="members/add" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-autumn animation-fadeIn">
                        <i class="fa fa-users sidebar-nav-icon"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        Members   <strong>(company)</strong><br>
<!--                        <small>Mountain Trip</small>-->
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
        <div class="col-sm-6 col-lg-3">
            <!-- Widget -->
            <a href="memberusers/add" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-spring animation-fadeIn">
                        <i class="fa fa-users sidebar-nav-icon"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        Members <strong>Users</strong><br>
<!--                        <small>Sales Today</small>-->
                    </h3>
                </div>
            </a>
            <!-- END Widget -->
        </div>
<!--        <div class="col-sm-6 col-lg-3">
             Widget 
            <a href="page_ready_inbox.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-fire animation-fadeIn">
                        <i class="gi gi-envelope"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        5 <strong>Messages</strong>
                        <small>Support Center</small>
                    </h3>
                </div>
            </a>
             END Widget 
        </div>
        <div class="col-sm-6 col-lg-3">
             Widget 
            <a href="page_comp_gallery.html" class="widget widget-hover-effect1">
                <div class="widget-simple">
                    <div class="widget-icon pull-left themed-background-amethyst animation-fadeIn">
                        <i class="gi gi-picture"></i>
                    </div>
                    <h3 class="widget-content text-right animation-pullDown">
                        +30 <strong>Photos</strong>
                        <small>Gallery</small>
                    </h3>
                </div>
            </a>
             END Widget 
        </div>-->
        
    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->
@stop