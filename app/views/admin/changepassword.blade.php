@extends('admin.masteradmin')

@section('title')
@parent
<title>Change Password</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-user-plus"></i>Change Password<br>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <!-- Edit Contact Block -->
            <div class="block">
                <!-- Edit Contact Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-danger enable-tooltip" data-toggle="block-hide" title="Remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
<form action="{{ Config::get('app.base_url') }}set-password" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" >
        <div class="form-group"></div>
            
	<div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-name">
			Current password
		</label>
		<div class="col-xs-9">
                    <input type="password" placeholder="Enter Current password" class="form-control" name='old_password' required> 
                            {{ $errors->first('old_password') }}
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-phone">
			New password
		</label>
		<div class="col-xs-9">
			<input type="password" placeholder="Enter New password" id="new_password" class="form-control" name='new_password' required> 
                            <span style="color:#a94442">{{ $errors->first('new_password') }}</span>
		</div>
	</div>
        <div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-email">
			Confirm new password
		</label>
		<div class="col-xs-9">
			<input type="password" placeholder="Enter Confirm new password"  class="form-control" name='new_password_confirmation' required> 
                            <span style="color:#a94442">{{ $errors->first('new_password_confirmation') }}</span>
		</div>
	</div>
	
	<div class="form-group form-actions">
		<div class="col-xs-9 col-xs-offset-3">
			<button type="submit" class="btn btn-md btn-primary pull-right">Save</button>
		</div>
	</div>
</form>
            </div>
        </div>
    </div>


</div>
<!-- END Page Content -->
@stop