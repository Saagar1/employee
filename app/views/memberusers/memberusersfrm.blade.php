@extends('admin.masteradmin')

@section('title')
@parent
<title>Member Users</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-user-plus"></i>Member Users Add/Edit<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ Config::get('app.base_url') }}manageusers">Manageusers</a></li>
        <li>Member Users Add/Edit</li>
    </ul>
    <div class="row">
        <div class="col-sm-12 col-md-12">
            
            <div class="block">
                
                <!-- Edit Contact Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-danger enable-tooltip" data-toggle="block-hide" title="Remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <!-- END Edit Contact Title -->
                
                <!-- Edit Contact Block -->
            
                <?php
                if ($param['action'] == 'add') {
                    $actionsubmit = 'Save';
                    $firstname = $lastname = $password = $emailid = $phonenumber = $type = '';
					$profileImage = Config::get('app.base_url').'admin/img/placeholders/avatars/avatar2.jpg';
                } else {
                    $actionsubmit = 'Update';
					$firstname = $param['memberuser'][0]->m_user_name;
					$lastname = $param['memberuser'][0]->m_user_cell;
					$emailid = $param['memberuser'][0]->m_user_email_id;
					$phonenumber = $param['memberuser'][0]->m_user_cell;
//					$type = $param['memberuser'][0]->type;
//					if($param['memberuser'][0]->profilepic == 0){
//						$profileImage = Config::get('app.base_url').'admin/img/placeholders/avatars/avatar2.jpg';
//					}else{
//						$profileImage = Config::get('app.base_url').'admin/img/Employee/'.$param['memberuser'][0]->empid.'.jpeg';
//					}
				}
                ?>
                @if(Session::get('flash_message') == 'success')
                <div class="row text-center">
                    <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">Successfully {{ $actionsubmit }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            @endif 
            @if(Session::get('flash_message') == 'error')
                <div class="row text-center">
                    <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-danger">{{ implode('', $errors->all('<div>:message</div>')) }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                </div>
            @endif 
                <!-- Edit Contact Content -->
<form action="{{ Config::get('app.base_url') }}postmemberuser/{{ $param['action'] }}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" >

        <div class="form-group"></div>
            
	<div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-name">
			Name
		</label>
		<div class="col-xs-9">
                    <input autocomplete="off" type="text" id="m_user_name" required="" name="m_user_name" class="form-control" value="{{ $firstname }}" placeholder="Enter Full Name..">
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-phone">
			Cell
		</label>
		<div class="col-xs-9">
                    <input autocomplete="off" type="text" required=""  id="m_user_cell" name="m_user_cell" class="form-control" value="{{ $phonenumber }}" placeholder="(000) 000-0000">
		</div>
	</div>
        <div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-email">
			Email
		</label>
		<div class="col-xs-9">
			<input autocomplete="off" type="email" required="" id="m_user_email_id" name="m_user_email_id" class="form-control" value="{{ $emailid }}" placeholder="Enter Email..">
		</div>
	</div>
	<div class="form-group">
		<label class="col-xs-3 control-label" for="edit-contact-name">
			Password
		</label>
		<div class="col-xs-9">
			<input autocomplete="off" type="password" id="m_user_password" name="m_user_password" class="form-control" placeholder="Enter Password..">
		</div>
	</div>
	
	<div class="form-group form-actions">
		<div class="col-xs-9 col-xs-offset-3">
			<button type="submit" class="btn btn-md btn-primary pull-right">{{ $actionsubmit }}</button>
		</div>
	</div>
</form>
            </div>
        </div>
    </div>


</div>
<!-- END Page Content -->
@stop