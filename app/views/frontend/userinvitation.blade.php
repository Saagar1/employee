@extends('frontend.masterhome')

@section('title')
@parent
<title>Clinet Exchange</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('js')
@parent
<!-- Load and execute javascript code used only in this page -->
<script src="{{ Config::get('app.base_url') }}admin/js/pages/formsWizard.js"></script>
<script>$(function () {
    FormsWizard.init();
});</script>
@stop


@section('content')
<section class="site-content site-section site-section-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="site-block">
                    <div class="site-block text-center">
                        <div class="block">
                            
                            <!-- Wizard with Validation Content -->
                            <form id="advanced-wizard" action="page_forms_wizard.html" method="post" class="form-horizontal form-bordered">
                                <!-- First Step -->
                                <div id="advanced-first" class="step">
                                    <h2 class="site-heading text-center">
                                        <strong>Account</strong> details
                                    </h2>
                                    <h4 class="text-center">
                                        Lets start by creating your account
                                    </h4>
                                    <!-- END Step Info -->
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" id="val_username" name="val_username" class="form-control" placeholder="Full Name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="input-group">
                                                <input type="password" id="val_password" name="val_password" class="form-control" placeholder="Password" required>
                                                <span class="input-group-addon">
                                                    <input type='checkbox'> Show
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" id="val_confirm_password" name="val_confirm_password" class="form-control" placeholder="Phone" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" id="val_username" name="val_username" class="form-control" placeholder="Company Name" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="input-group">
                                                <input type="password" id="val_password" name="val_password" class="form-control" placeholder="Subdomain Name" required>
                                                <span class="input-group-addon">
                                                    .clientexchange.net
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <select class="form-control">
                                                <option>Select currency</option>
                                                <option>USD</option>
                                                <option>INR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <!-- END First Step -->

                                <!-- Second Step -->
                                <div id="advanced-second" class="step">
                                    <h2 class="site-heading text-center">
                                        <strong>Team</strong> details
                                    </h2>
                                    <h4 class="text-center">
                                        How many people are there in your team?
                                    </h4>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="">
                                                <input type="button" class="btn btn-alt btn-info" value="1-5" style="border: 1px solid rgb(194, 194, 194);">
                                                <input type="button" class="btn btn-alt btn-info" value="6-15" style="border: 1px solid rgb(194, 194, 194);">
                                                <input type="button" class="btn btn-alt btn-info" value="6-50" style="border: 1px solid rgb(194, 194, 194);">
                                                <input type="button" class="btn btn-alt btn-info" value="50+" style="border: 1px solid rgb(194, 194, 194);">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-2">
                                            <input type="text" id="val_confirm_password" name="val_confirm_password" class="form-control" placeholder="Your website address" required>
                                        </div>
                                    </div>
                                </div>
                                <!-- END Second Step -->

                                <!-- Form Buttons -->
                                <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-4">
                                        <input type="reset" class="btn btn-sm btn-warning" id="back2" value="Back">
                                        <input type="submit" class="btn btn-sm btn-primary" id="next2" value="Next">
                                    </div>
                                </div>
                                <!-- END Form Buttons -->
                            </form>
                            <!-- END Wizard with Validation Content -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6" style='background: #e2435d;'>
                <img alt="Step_1" class="img-responsive" src="https://dziaodg7d4has.cloudfront.net/assets/signup/step_1-db53d6d04a23cda612ffe2afa72102a8.png">
            </div>
        </div>
    </div>
</section>
@stop