@extends('frontend.masterhome')

@section('title')
@parent
<title>Clinet Exchange</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<section class="site-content site-section site-section-top">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="site-block">
                    <h2 class="site-heading text-center">
                        <strong>You don't need</strong> a project management tool
                    </h2>
                    <h4 class="text-center">
                        The best tool is the one your team actually uses and dapulse comes with built-in addiction
                    </h4>
                    <p></p>
                    <div class="site-block text-center">
                        <form action="{{ Config::get('app.base_url') }}signup" method="post" class="form-horizontal" >
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-3">
                                    <label class="sr-only" for="register-email">
                                        Enter your work email
                                    </label>
                                    <div class="input-group input-group-lg">
                                        <input type="email" id="register-email" name="register-email" class="form-control" placeholder="Enter your work email">
                                        <input type="hidden" id="user-id" name="user-id" value="10000" class="form-control" placeholder="Enter your work email">
                                        <div class="input-group-btn">
                                            <button type="submit" class="btn btn-primary">
                                                <i class="fa fa-plus"></i> Create your team
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@stop