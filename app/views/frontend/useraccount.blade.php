@extends('frontend.masterhome')

@section('title')
@parent
<title>Clinet Exchange</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/main1.css">

<link href="{{ Config::get('app.base_url') }}frontend/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="{{ Config::get('app.base_url') }}frontend/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">

@section('content')

<div class="col-sm-6 col-md-6 site-block col-md-offset-3" style="margin-top:50px">
    <h3 class="h2 site-heading text-center"><strong>Customise</strong> Profile</h3>
    <h5 class="h5 site-heading text-center">Ensure that you only view leads related to market</h5>
    <form action="contact.html#form-contact" method="post" id="form-contact">
        <div class="form-group">
            <label class="col-md-12 control-label" for="example-colorpicker4">Add Your Industry Keyword <span class="text-muted fa-1x">Ensure to add correct info</span> </label>
            <div class="col-md-12">
                <input name="tags" id="industry" value="Apple, Orange" class="form-control input-lg" placeholder="Type of industry keyword">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-12 control-label" for="example-colorpicker4">Add Your Sub Industry Keyword <span class="text-muted fa-1x">Ensure your correct info</span></label>
            <div class="col-md-12">
                <input name="tags" id="subindustry" value="" class="form-control input-lg" placeholder="Type of industry keyword">
            </div>
            
        </div>
        <div class="form-group">
            <label class="col-md-12 control-label" for="example-colorpicker4">Service Location</label>
            <div class="col-md-12">
                <input name="tags" id="" value="" class="form-control input-lg">
            </div>
            
        </div>
        <div class="form-group form-actions">
            <div class="row col-md-12">
                <br/><br/>
                <button type="submit" class="btn btn-lg btn-primary pull-right"><i class="fa fa-lock"></i> Save</button>
            </div>

        </div>
    </form>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>

<!-- The real deal -->
<script src="{{ Config::get('app.base_url') }}frontend/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<script>
$(function () {
    var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

    $('#industry').tagit({
        availableTags: sampleTags,
        // This will make Tag-it submit a single form value, as a comma-delimited field.
        singleField: true,
    });
    
    $('#subindustry').tagit({
        availableTags: sampleTags,
        // This will make Tag-it submit a single form value, as a comma-delimited field.
        singleField: true,
    });
});
</script>    


@stop