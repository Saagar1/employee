@extends('frontend.masterhome')

@section('title')
@parent
<title>Clinet Exchange</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop
<link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/main.css">
@section('content')
<body>
    <!-- Coming Soon Background -->
    <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
    <img src="{{ Config::get('app.base_url') }}frontend/img/placeholders/photos/coming_soon_full_bg.jpg" alt="Coming Soon Background" class="animation-pulseSlow full-bg">
    <!-- END Coming Soon Background -->

    <!-- Coming Soon -->
    <div class="full-page-container text-center">
        <!-- Title -->
        <h1 class="text-light"><i class="gi gi-message_flag  fa-2x"></i></h1>
        <h1 class="text-center animation-slideDown" style="color:white"><strong>Check your email</strong></h1>

        <!-- END Title -->

        <!-- Subscribe Form -->
        <h6 class="h4 text-light push" style="color:white">We sent message to {{ $param['emailId'] }}</h6>
        <h6 class="h4 text-light push" style="color:white">Please confirm your message to complete registration</h6><br/>
        <button type="button" class="btn btn-lg btn-warning">Check my inbox</button><br/><br/>
        <!-- END Subscribe Form -->
        <hr/>
        <p style="color:white"> Email not received <a href="#">Send Again</a> 
        
    </div>
    <!-- END Coming Soon -->

</body>
@stop