@extends('admin.masteradmin')

@section('title')
@parent
<title>Members (company)</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<!-- Page content -->
<div id="page-content">
    <!-- Blank Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="fa fa-user-plus"></i>Member (Company) Add/Edit<br>
            </h1>
        </div>
    </div>
    <ul class="breadcrumb breadcrumb-top">
        <li><a href="{{ Config::get('app.base_url') }}manageusers">Manageusers</a></li>
        <li>Members Add/Edit</li>
    </ul>
    <div class="row">
        <div class="col-sm-12 col-md-6 col-md-offset-3">

            <div class="block">

                <!-- Edit Contact Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        <a href="javascript:void(0)" class="btn btn-sm btn-alt btn-danger enable-tooltip" data-toggle="block-hide" title="Remove"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <!-- END Edit Contact Title -->

                <!-- Edit Contact Block -->

                <?php
                if ($param['action'] == 'add') {
                    $actionsubmit = 'Save';
                    $m_compny_name = $m_team_size = '';
                    $profileImage = Config::get('app.base_url') . 'admin/img/placeholders/avatars/avatar2.jpg';
                } else {
                    $actionsubmit = 'Update';
                    $m_compny_name = $param['membersCompny'][0]->m_compny_name;
                    $m_team_size = $param['membersCompny'][0]->m_team_size;
                    
//					$type = $param['membersCompny'][0]->type;
//					if($param['membersCompny'][0]->profilepic == 0){
//						$profileImage = Config::get('app.base_url').'admin/img/placeholders/avatars/avatar2.jpg';
//					}else{
//						$profileImage = Config::get('app.base_url').'admin/img/Employee/'.$param['membersCompny'][0]->empid.'.jpeg';
//					}
                }
                ?>

                <!-- Edit Contact Content -->
                <form action="{{ Config::get('app.base_url') }}members/{{ $param['action'] }}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" >

                    @if(Session::get('flash_message') == 'success')
                    <div class="form-group"></div>
                    <div class="col-md-12 text-center">
                        <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">Successfully {{ $actionsubmit }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    @endif 
                    @if(Session::get('flash_message') == 'error')
                    <div class="form-group"></div>
                    <div class="col-md-12 text-center">
                        <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-danger">{{ implode('', $errors->all('<div>:message</div>')) }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                    </div>
                    @endif 
                    <div class="form-group"></div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="edit-contact-name">
                            Company Name
                        </label>
                        <div class="col-xs-9">
                            <input autocomplete="off" type="text" id="company_name" required="" name="company_name" class="form-control" value="{{ $m_compny_name }}" placeholder="Enter Company Name..">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="edit-contact-phone">
                            Team Size (no of Employees)
                        </label>
                        <div class="col-xs-9">
                            <select id="team_size" name="team_size" class="form-control" size="1">
                                <option value="">Please select</option>
                                <option value="1-5" @if($m_team_size == '1-5') selected @endif>1-5</option>
                                <option value="6-15" @if($m_team_size == '6-15') selected @endif>6-15</option>
                                <option value="6-50" @if($m_team_size == '6-50') selected @endif>6-50</option>
                                <option value="50+" @if($m_team_size == '50+') selected @endif>50+</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group form-actions">
                        <div class="col-xs-9 col-xs-offset-3">
                            <button type="submit" class="btn btn-md btn-primary pull-right">{{ $actionsubmit }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<!-- END Page Content -->
@stop