@extends('backend.master')

@section('title')
@parent
<title>User Account</title>
@stop

@section('description')
@parent
<meta content="description here" name="description" />
@stop

@section('content')
<!-- Page content -->
<div id="page-content">

    <!-- Example Block -->
    <div class="block">
        <!-- Example Title -->
        <div class="block-title">
            <h2>User Account</h2>
        </div>
        <?php
        if ($param['action'] == 'add') {
            $actionsubmit = 'Save';
            $m_compny_name = $m_team_size = '';
            $profileImage = Config::get('app.base_url') . 'admin/img/placeholders/avatars/avatar2.jpg';
        } else {
            $actionsubmit = 'Update';
            $m_compny_name = $param['membersCompny'][0]->m_compny_name;
            $m_team_size = $param['membersCompny'][0]->m_team_size;

//					$type = $param['membersCompny'][0]->type;
//					if($param['membersCompny'][0]->profilepic == 0){
//						$profileImage = Config::get('app.base_url').'admin/img/placeholders/avatars/avatar2.jpg';
//					}else{
//						$profileImage = Config::get('app.base_url').'admin/img/Employee/'.$param['membersCompny'][0]->empid.'.jpeg';
//					}
        }
        ?>
        <!-- END Example Title -->
        <form action="{{ Config::get('app.base_url') }}members/{{ $param['action'] }}" method="post" class="form-horizontal form-bordered" enctype="multipart/form-data" >

            @if(Session::get('flash_message') == 'success')
            <div class="form-group"></div>
            <div class="col-md-12 text-center">
                <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-success">Successfully {{ $actionsubmit }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            </div>
            @endif 
            @if(Session::get('flash_message') == 'error')
            <div class="form-group"></div>
            <div class="col-md-12 text-center">
                <div id="alertmessagebox" role="alert" class="alert alert-dismissible alert-danger">{{ implode('', $errors->all('<div>:message</div>')) }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
            </div>
            @endif 
            <div class="form-group"></div>


            <div class="form-group">
                <label class="col-xs-3 control-label" for="edit-contact-name">
                    Add Your Industry Keyword <i class="fa fa-wrench"></i><br/> 
                    <span class="text-muted fa-1x">Be thorought, add multiple</span> 
                </label>
                <div class="col-xs-9">
                    <input autocomplete="off" type="text" id="industry" required="" name="company_name" class="form-control" value="" placeholder="Enter Company Name..">
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-3 control-label" for="edit-contact-phone">
                    Service Location <i class="fa fa-map-marker"></i>
                </label>
                <div class="col-xs-6">
                    <input autocomplete="off" type="text" id="autocomplete" required="" name="company_name" class="form-control" value="" placeholder="Enter Service Location.." onFocus="geolocate()"></input>
                    <span class="text-muted fa-1x">Add big or multiple location which will cover your entire service area include your branches addresses</span> 
                    <input type="hidden" class="field" id="street_number" disabled="true"></input>
                    <input type="hidden" class="field" id="route" disabled="true"></input>
                    <input type="hidden" class="field" id="locality" disabled="true"></input>
                    <input type="hidden" class="field" id="administrative_area_level_1" disabled="true">
                    <input type="hidden" class="field" id="postal_code" disabled="true"></input>
                    <input type="hidden" class="field" id="country" disabled="true"></input>
                </div>
                <div class="col-xs-3">
                    <input type="button" value="Add Location" class="btn btn-success" id="member_service_location">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-8 col-xs-offset-3">
                    <?php
                    try {
                        $service_location = $param['service_location'];
                        if (!empty($service_location)) {
                            ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Your Service areas</th>
                                        <th>Location Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($service_location as $value) {
                                        ?>


                                        <tr>
                                            <td>{{ $value->mem_address }}</td>
                                            <td><input type="text" value="{{ $value->location_description }}"></td>
                                            <td><i class="fa fa-pencil"></i> | <i class="fa fa-times-circle"></i></td>
                                        </tr>

                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <?php
                        }
                    } catch (Exception $ex) {
                        echo $ex;
                    }
                    ?>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-xs-9 col-xs-offset-3">
                    <button type="submit" class="btn btn-md btn-primary pull-right">{{ $actionsubmit }}</button>
                </div>
            </div>
        </form>   

    </div>
    <!-- END Example Block -->
</div>
<!-- END Page Content -->
<input type="hidden" value="{{ Config::get('app.base_url') }}" id="baseUrl">
@stop

@section('js')
@parent
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<link href="{{ Config::get('app.base_url') }}frontend/css/jquery.tagit.css" rel="stylesheet" type="text/css">
<link href="{{ Config::get('app.base_url') }}frontend/css/tagit.ui-zendesk.css" rel="stylesheet" type="text/css">
<!-- The real deal -->
<script src="{{ Config::get('app.base_url') }}frontend/js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<script>
                        // Tag it script
                        $(function () {
                            var base_url = $('#baseUrl').val();
                            var sampleTags = ['c++', 'java', 'php', 'coldfusion', 'javascript', 'asp', 'ruby', 'python', 'c', 'scala', 'groovy', 'haskell', 'perl', 'erlang', 'apl', 'cobol', 'go', 'lua'];

                            $('#industry').tagit({
                                availableTags: sampleTags,
                                // This will make Tag-it submit a single form value, as a comma-delimited field.
                                singleField: true,
                            });
                        });

                        // Location script                    
                        var placeSearch, autocomplete;
                        var componentForm = {
                            street_number: 'short_name',
                            route: 'long_name',
                            locality: 'long_name',
                            administrative_area_level_1: 'long_name',
                            country: 'long_name',
                            postal_code: 'long_name'
                        };

                        function initAutocomplete() {
                            // Create the autocomplete object, restricting the search to geographical
                            // location types.
                            autocomplete = new google.maps.places.Autocomplete(
                                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                    {types: ['geocode']});

                            // When the user selects an address from the dropdown, populate the address
                            // fields in the form.
                            autocomplete.addListener('place_changed', fillInAddress);
                        }

                        function fillInAddress() {
                            // Get the place details from the autocomplete object.
                            var place = autocomplete.getPlace();

                            for (var component in componentForm) {
                                document.getElementById(component).value = '';
                                document.getElementById(component).disabled = false;
                            }

                            // Get each component of the address from the place details
                            // and fill the corresponding field on the form.
                            for (var i = 0; i < place.address_components.length; i++) {
                                var addressType = place.address_components[i].types[0];
                                if (componentForm[addressType]) {
                                    var val = place.address_components[i][componentForm[addressType]];
                                    document.getElementById(addressType).value = val;
                                }
                            }
                        }

                        // Bias the autocomplete object to the user's geographical location,
                        // as supplied by the browser's 'navigator.geolocation' object.
                        function geolocate() {
                            if (navigator.geolocation) {
                                navigator.geolocation.getCurrentPosition(function (position) {
                                    var geolocation = {
                                        lat: position.coords.latitude,
                                        lng: position.coords.longitude
                                    };
                                    var circle = new google.maps.Circle({
                                        center: geolocation,
                                        radius: position.coords.accuracy
                                    });
                                    autocomplete.setBounds(circle.getBounds());
                                });
                            }
                        }


                        /* ajax coding start */
                        $(document).on('click', '#member_service_location', function () {
                            var mem_address = $('#autocomplete').val();
                            var mem_city = $('#locality').val();
                            var mem_state = $('#administrative_area_level_1').val();
                            var mem_zip_code = $('#postal_code').val();
                            var mem_country = $('#country').val();
                            $.post('{{ Config::get('app.base_url') }}' + 'memservicelocation', {
                                mem_address: mem_address,
                                mem_city: mem_city,
                                mem_state: mem_state,
                                mem_zip_code: mem_zip_code,
                                mem_country: mem_country
                            }, function (data) {
                                console.log(data);
                            })
                        });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBawTCAVn1QTy3q7zCMnaj1mdZcps1RCxI&libraries=places&callback=initAutocomplete"
async defer></script>
@stop