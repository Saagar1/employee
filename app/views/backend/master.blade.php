<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <head>
        @yield('title')
        @yield('description')

        <meta name="description" content="ProUI is a Responsive Bootstrap Admin Template created by pixelcave and published on Themeforest.">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ Config::get('app.base_url') }}admin/img/favicon.png">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="{{ Config::get('app.base_url') }}admin/img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/bootstrap.min.css">

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/main.css">

        <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ Config::get('app.base_url') }}admin/css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (enables responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{ Config::get('app.base_url') }}admin/js/vendor/modernizr-respond.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>


        @yield('css')

        <style>
			.popover { z-index : 900; }
			.datepicker { z-index: 999; }
            .popover-content {
                padding: 6px 7px;
            }
            .popover {
                max-width: 60%;
                //mix-width: 200px;
                //width: auto;
            }
            #comments tr td {
                padding: 4 !important;
            }
            #comments thead tr td {
                padding: 4 !important;
                background-color: #eaedf1;
            }
            .textsize{
                font-size: 1.3em;
            }
            .tablehead{
                font-size: 14px !important;
                padding-top: 6px !important;
                padding-bottom: 6px !important;
                text-align: center;
            }
         </style>

        @yield('js')
        <link rel="shortcut icon" href="favicon.ico" />
    </head>

    <body>
        <input type="text" value="{{ Config::get('app.base_url') }}" id="base_url">
        <div id="page-wrapper">
            <div class="preloader themed-background">
                <h1 class="push-top-bottom text-light text-center"><strong>Pro</strong>UI</h1>
                <div class="inner">
                    <h3 class="text-light visible-lt-ie9 visible-lt-ie10"><strong>Loading..</strong></h3>
                    <div class="preloader-spinner hidden-lt-ie9 hidden-lt-ie10"></div>
                </div>
            </div>
            <div id="page-container" class="sidebar-mini sidebar-visible-lg-mini sidebar-no-animations">
                <!-- Main Sidebar -->
                <div id="sidebar">
                    <!-- Wrapper for scrolling functionality -->
                    <div id="sidebar-scroll">
                        <!-- Sidebar Content -->
                        <div class="sidebar-content">
                            <!-- Brand -->
                            <a href="{{ Config::get('app.base_url') }}interacting" class="sidebar-brand">
                                <i class="gi gi-flash"></i><span class="sidebar-nav-mini-hide"><strong>Grass</strong>wholesalers</span>
                            </a>
                            <!-- END Brand -->
                            <?php
                            $profileImage = '';
                            ?>	
                            <!-- User Info -->
                            <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                                <div class="sidebar-user-avatar">
                                    <a href="page_ready_user_profile.html">
                                        <img src="{{ $profileImage }}">
                                    </a>
                                </div>
                                <div class="sidebar-user-name" style="font-size:15px">
                                    <?php
                                    if (Auth::admin()->check()) {
                                        ?>
                                        {{ Auth::admin()->get()->email }}
                                        <?php
                                    } else if (Auth::member_users()->check()) {
                                        ?>
                                        {{ Auth::member_users()->get()->firstname }} {{ Auth::member_users()->get()->lastname }}
                                        <?php
                                    }
                                    ?>	
                                </div>
                                <div class="sidebar-user-links">
                                    <a href="page_ready_user_profile.html" data-toggle="tooltip" data-placement="bottom" title="Profile"><i class="gi gi-user"></i></a>
                                    <a href="login.html" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                                </div>
                            </div>
                            <ul class="sidebar-nav">
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}dashboard" class="<?php echo $param['leftmenu'] == 'dashboard' ? 'active' : ''; ?>">
                                        <i class="fa fa-tachometer sidebar-nav-icon"></i>
                                        <span class="sidebar-nav-mini-hide">Dashboard</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}useraccount" class="<?php echo $param['leftmenu'] == 'useraccount' ? 'active' : ''; ?>">
                                        <i class="fa fa-users sidebar-nav-icon"></i>
                                        <span class="sidebar-nav-mini-hide">User Account</span>
                                    </a>
                                </li>
                                <?php
                                if (Auth::admin()->check()) {
                                    ?>
                                    <li>
                                        <a href="{{ Config::get('app.base_url') }}manageusers" class="<?php echo $param['leftmenu'] == 'manageusers' ? 'active' : ''; ?>">
                                            <i class="fa fa-users sidebar-nav-icon"></i>
                                            <span class="sidebar-nav-mini-hide">Manage Users</span>
                                        </a>
                                    </li>
                                    <?php
                                }
                                ?>
                                <li>
                                    <a href="{{ Config::get('app.base_url') }}logout">
                                        <i class="fa fa-power-off sidebar-nav-icon"></i>
                                        <span class="sidebar-nav-mini-hide">Logout</span>
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                
                <div id="main-container">
                   <header class="navbar navbar-default">
                        <ul class="nav navbar-nav-custom">
                            <li>
                                <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');
                                        this.blur();">
                                    <i class="fa fa-bars fa-fw"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Right Header Navigation -->
                        <ul class="nav navbar-nav-custom pull-right">
                            <li>
                               
                            </li>
                            
                            <!-- User Dropdown -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php
                                    $pic = 'no_user.gif'; 
                                    $emailId = 'user.test@gmail.com'
                                    ?>
                                    {{ $emailId }} <img src="{{Config::get('app.base_url') . 'admin/img/'.$pic}}" alt="avatar"> <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                                    <li>
                                        @if (Auth::admin()->check())
                                        <a href="{{Config::get('app.base_url')}}manageusers">
                                            Settings
                                        </a>
                                        @endif
                                        <a href="{{Config::get('app.base_url')}}logout">
                                            Logout
                                        </a>
                                        
                                    </li>
                                </ul>
                            </li>
                            <!-- END User Dropdown -->
                        </ul>
                        <!-- END Right Header Navigation -->
                    </header>
                    
                    @yield('content')
                   

                    <!-- Footer -->
                    <footer class="clearfix">
                        <div class="pull-right">
                            Crafted with <i class="fa fa-heart text-danger"></i> by <a href="http://goo.gl/vNS3I" target="_blank">pixelcave</a>
                        </div>
                        <div class="pull-left">
                            <span id="year-copy"></span> &copy; <a href="http://goo.gl/TDOSuC" target="_blank">ProUI 3.1</a>
                        </div>
                    </footer>
                    <!-- END Footer -->
                </div>
                <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
        </div>
        <!-- END Page Wrapper -->

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->

    </div>
    <!-- END User Settings -->
    <input type="hidden" value="{{ Config::get('app.base_url') }}" id="baseUrl">
    <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file -->
    
    <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.2.min.js"%3E%3C/script%3E'));</script>
    
    <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
    <script src="{{ Config::get('app.base_url') }}admin/js/vendor/bootstrap.min.js"></script>
    <script src="{{ Config::get('app.base_url') }}admin/js/plugins.js"></script>
    <script src="{{ Config::get('app.base_url') }}admin/js/app.js"></script>
    <script src="{{ Config::get('app.base_url') }}admin/js/pages/tablesDatatables.js"></script>
    <script src="{{ Config::get('app.base_url') }}admin/js/grasswholesalers.js?v=1.3"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script>$(function () {
    TablesDatatables.init();
});</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.textcomplete/1.6.2/jquery.textcomplete.min.js"></script>
    <!-- Button trigger modal -->
</body>
</html>
