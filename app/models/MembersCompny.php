<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
 
class MembersCompny extends Eloquent implements UserInterface, RemindableInterface {
	protected $table = 'members_compny';
        protected $primaryKey = 'm_compny_id';
    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }
 
    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return $this->remember_token;
    }
 
    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }
 
    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }
 
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }
    
    public static function postMemberCompny($data) {
        DB::table('members_compny')->insert($data);
        return DB::getPdo()->lastInsertId();
    }
    
    public static function getMemberCompny($data){
        $result = DB::table('members_compny');
        if (!empty($data)) {
            $result->where($data);
        }
        return $result->get();
    }
    
    public static function putMemberCompny($id,$data) {
        $result = DB::table('members_compny');
        if (!empty($id)) {
            $result->where($id);
        }
		$result->update($data);
        //dd(DB::getQueryLog());
    }
    
     public static function memberUserServiceLocation($data){
         DB::table('member_user_service_location')->insert($data);
        return DB::getPdo()->lastInsertId();
        
    }
    
}   
