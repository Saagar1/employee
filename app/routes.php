<?php

Route::group(array('before' => 'auth'), function() {
    Route::get('dashboard', 'AdminController@getAdminDashboard');
    Route::get('memberusers/{action}', 'AdminController@getMemberUsers');
    Route::get('manageusers', 'AdminController@getManageUsers');
    Route::post('postmemberuser/{action}', 'MemberUsersController@postMemberUser');
    
    Route::get('change-password', 'AdminController@getChangePassword');

    //members company
    Route::get('members/{action}', 'AdminController@getMemberCompny');
    Route::post('members/{action}', 'MembersCompnyController@postMemberCompny');
    

});

Route::group(array('before' => 'isMemberUsers'), function() {
    
});



//Route::get('/', function() {
//    if (!Auth::check()) {
//        return View::make('hello');
//    } else {
//        return Redirect::to('home');
//    }
//});

Route::get('login', 'MemberUsersController@getLogin');
Route::post('login', 'MemberUsersController@postLogin');
Route::get('logout', 'MemberUsersController@getLogout');

// member user section
Route::get('memberuseractivate/{emailid}/{id}', 'MemberUsersController@getMemberUserActivate');
Route::get('setpassword', 'AdminController@getSetPassword');
Route::post('setpassword', 'AdminController@postSetPassword');
Route::post('signup', 'FrontendController@postSignupEmail');
Route::get('signup/waiting_for_confirm', 'FrontendController@getWaitingForConfirm');
Route::get('board', 'FrontendController@getTeamUp');
Route::get('useraccount', 'MembersCompnyController@getUserAccountForm');

Route::post('memservicelocation', 'MembersCompnyController@postMemServiceLocation');

// Frontend Section
Route::get('/', 'FrontendController@getHomePage');
Route::get('users/invitation/{invitationtoken}', 'FrontendController@getUserInvitationAccept');