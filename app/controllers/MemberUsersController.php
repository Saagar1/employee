<?php

class MemberUsersController extends BaseController {
    
    public function getLogin() {
        return View::make('admin.login');
    }
    
    public function postMemberUser($param) {
        try {
            $unique = '';
            if ($param == 'add') {
                $unique = 'unique:member_users';
            }
            $credentials = [
                'm_user_name' => Input::get('m_user_name'),
                //'password' => Input::get('m_user_password'),
                'm_user_email_id' => Input::get('m_user_email_id'),
                'm_user_cell' => Input::get('m_user_cell')
            ];

            $rules = array(
                'm_user_name' => 'required',
                //'password' => 'required',
                'm_user_cell' => 'required',
                'm_user_email_id' => 'required|email|' . $unique // required and must be unique in the ducks table
            );

            $validator = Validator::make($credentials, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::back()->withFlashMessage('error')->withErrors($validator);
            } else {

                $m_user_name = trim(Input::get('m_user_name'));
                $m_user_cell = trim(Input::get('m_user_cell'));
                $m_user_email_id = trim(Input::get('m_user_email_id'));
                $m_user_password = $password = trim(Input::get('m_user_password'));

                if ($param == 'add') {

                    if (!empty($m_user_password)) {
                        $m_user_password = Hash::make($m_user_password);
                    }
                    $m_user_uuid = App::make('HomeController')->generate_uuid();
                    $data = array(
                        'm_user_uuid' => $m_user_uuid,
                        'm_user_name' => $m_user_name,
                        'm_user_cell' => $m_user_cell,
                        'm_user_email_id' => $m_user_email_id,
                        'password' => $m_user_password,
                        'm_user_ip' => $_SERVER['REMOTE_ADDR'],
                        'm_user_string' => $_SERVER["HTTP_USER_AGENT"],
                        'm_user_time' => time()
                    );
                    $last_id = MemberUser::postMemberUser($data);

                    $data['message'] = 'Hi  ' . $m_user_name . '
                            Please click on the below link to activate your account: 
                            <a href="' . Config::get('app.base_url') . 'memberuseractivate/' . base64_encode($m_user_email_id) . '/' . base64_encode($last_id) . '/'.base64_encode($password).'">Click Here</a>

                            Best Regards
                            Client Exchange Team';

                    $responceMessage = Mail::send('emailview', array('param' => $data), function($message) use($data) {
                                $message->from('NoReply@clientexchange.net', 'ClientExchange')->subject('Client Exchange Activation');
                                $message->to($data['m_user_email_id'], $data['m_user_name']);
                            });
                } else {
                    $update_data = array(
                        'm_user_name' => $m_user_name,
                        'm_user_cell' => $m_user_cell,
                        'm_user_email_id' => $m_user_email_id,
                        'password' => $m_user_password
                    );
                    $data_id = array(
                        'm_user_id' => $param,
                    );
                    $last_id = MemberUser::putMemberUser($data_id, $update_data);
                }
            }
            $responce = 'success';
            return Redirect::back()->withFlashMessage($responce);
        } catch (Exception $ex) {
            $type = 'postMemberUser';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

    public function getMemberUserActivate($email_id, $id) {
        //echo base64_encode($email_id).' ~ '.base64_encode($id);die;
        try {
            $email_id = base64_decode($email_id);
            $id = base64_decode($id);
            //echo $email_id;die;
            $data = array(
                'm_user_email_id' => $email_id,
                'm_user_id' => $id
            );
            $member_user = MemberUser::getMemberUser($data);

            if (!empty($member_user)) {
                /* update column */
                $update_data = array(
                    'm_user_is_email_verify' => 1
                );
                $data_id = array(
                    'm_user_id' => $id
                );
                MemberUser::putMemberUser($data_id, $update_data);
                
                // set session value
                Session::put('memberUserActivateEmail',$email_id);
                Session::put('memberUserActivateId',$id);
                
                if (empty($member_user[0]->password)) {
                    return Redirect::to('setpassword');
                } else {
                    return Redirect::to('login');
                }
            } else {
                echo 'no data';
            }
        } catch (Exception $ex) {
            $type = 'getMemberUserActivate';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }

}
