<?php

class FrontendController extends BaseController {
    
    public function getHomePage(){
        return View::make('frontend.home');
    }
    public function postSignupEmail(){
        $registerEmail = Input::get('register-email');
        $userId = Input::get('user-id');
        $param = array(
            'emailId' => $registerEmail,
            'user-id' => $userId
        );
        //return Redirect::to('signup/waiting_for_confirm?email='.$registerEmail.'&user_id='.$userId);
        return Redirect::to('signup/waiting_for_confirm?email='.$registerEmail.'&user_id='.$userId);
    }
    
    public function getUserInvitationAccept($invitationtoken){
        return View::make('frontend.userinvitation');
    }
    
    public function getWaitingForConfirm(){
        $registerEmail = Input::get('email');
        $userId = Input::get('user-id');
        $param = array(
            'emailId' => $registerEmail
        );
        return View::make('frontend.confirmemailscreen', array('param' => $param));
    }
    
    public function getTeamUp(){
        return View::make('frontend.teaminvite');
    }
    
}
