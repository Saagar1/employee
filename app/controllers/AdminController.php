    <?php

class AdminController extends BaseController
{

    public function getAdminLogin() {
        return View::make('admin.login');
    }

    public function postAdminLogin() {
        $credentials = [
            'email' => Input::get('login-email'),
            'password' => Input::get('login-password')
        ];
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->passes()) {
            if (trim(Input::get('type')) == 0) {
                if (Auth::admin()->attempt($credentials)) {
                    Session::put('isAdmin', true);
                    return Redirect::to('dashboard');
                }
            } else if (trim(Input::get('type')) == 1 || trim(Input::get('type')) == 2) {
                if (Auth::employee()->attempt($credentials)) {
                    Session::put('isAdmin', false);
                    return Redirect::to('dashboard');
                }
            }
            return Redirect::back()->withInput()->withFlashMessage('invalidpassword');
        }
        return Redirect::back()->withErrors($validator)->withInput();
    }

    public function getAdminDashboard() {
        $param = array(
            'leftmenu' => 'dashboard'
        );
        return View::make('admin.dashboard', array('param' => $param));
    }

    public function getAdminLogout() {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
    
    public function getManageUsers() {
        $param = array(
            'leftmenu' => 'manageusers'
        );
        return View::make('admin.manageusers', array('param' => $param));
    }
    
    public function getMemberUsers($param) {
        
        $memberuser = '';
        if($param != 'add'){
            $data_id = array(
                'm_user_id' => $param
            );
            $memberuser = MemberUser::getMemberUser($data_id);
        }
        $param = array(
            'leftmenu' => 'manageusers',
            'action' => $param,
            'memberuser' => $memberuser
        );
        
        return View::make('memberusers.memberusersfrm', array('param' => $param));
    }
    
    public function getChangePassword(){
        $param = array(
            'leftmenu' => 'manageusers'
        );
        return View::make('admin.changepassword', array('param' => $param));
    }
    
    public function getSetPassword(){
        $param = array(
            'leftmenu' => 'manageusers'
        );
        return View::make('admin.setpassword', array('param' => $param));
    }
    
    public function postSetPassword(){
        try{
            $rules = array(
                'new_password' => 'required|confirmed',
                'new_password_confirmation' => 'required|same:new_password'
            );
            $id = Session::get('memberUserActivateId');
            $user = MemberUser::find($id);
            $validator = Validator::make(Input::all(), $rules);
            //Is the input valid? new_password confirmed and meets requirements
            if ($validator->fails()) {
                return Redirect::back()->withInput()->withErrors($validator->messages());
            }
            $newPassword = $_POST['new_password'];
            
            // new password update
            $update_data = array(
                'password' => Hash::make($newPassword)
            );
            $data_id = array(
              'm_user_id' => $id
            );
            MemberUser::putMemberUser($data_id,$update_data);
            
            // remove sesion that set on : MemberUsersController
            Session::forget('memberUserActivateId');
            Session::forget('memberUserActivateEmail');
            
            return Redirect::to('login');
        } catch (Exception $ex) {
            $type = 'postSetPassword';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }
    
    
    public function getMemberCompny($param) {
        
    $membersCompny = '';
        if($param != 'add'){
            $data_id = array(
                'm_compny_id' => $param
            );
            $membersCompny = MembersCompny::getMemberCompny($data_id);
        }
        $param = array(
            'leftmenu' => 'manageusers',
            'action' => $param,
            'membersCompny' => $membersCompny
        );
        
        return View::make('memberscompny.memberscompnyfrm', array('param' => $param));
    }
    
}
