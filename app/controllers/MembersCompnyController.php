<?php

class MembersCompnyController extends BaseController {

    public function postMemberCompny($param) {
        try {
            $unique = '';
            if($param == 'add'){
                $unique =  'unique:members_compny';
            }
            $credentials = [
                'm_compny_name' => Input::get('company_name'),
            ];

            $rules = array(
                'm_compny_name' => 'required|'.$unique // required and must be unique in the ducks table
            );

            $validator = Validator::make($credentials, $rules);

            if ($validator->fails()) {
                $messages = $validator->messages();
                return Redirect::back()->withFlashMessage('error')->withErrors($validator);
            } else {
                
                $company_name = trim(Input::get('company_name'));
                $team_size = trim(Input::get('team_size'));
                
                if($param == 'add'){
                
                if (!empty($m_user_password)) {
                    $m_user_password = Hash::make($m_user_password);
                }
                    $m_user_uuid = 'Membrs-'.App::make('HomeController')->generate_uuid();
                    $data = array(
                        'm_compny_uuid' => $m_user_uuid,
                        'm_compny_name' => $company_name,
                        'm_team_size' => $team_size,
                        'membrs_ip' => $_SERVER['REMOTE_ADDR'],
                        'membrs_string' => $_SERVER["HTTP_USER_AGENT"],
                        'membrs_time' => time()
                    );
                    $last_id = MembersCompny::postMemberCompny($data);
                    
//                    $data['message'] = 'Hi  '.$m_user_name.'
//                            Please click on the below link to activate your account: 
//                            <a href="'.Config::get('app.base_url').'memberuseractivate/'.base64_encode($m_user_email_id).'/'.base64_encode($last_id).'">Click Here</a>
//
//                            Best Regards
//                            Client Exchange Team';
//                
//                    $responceMessage = Mail::send('emailview', array('param' => $data), function($message) use($data) {
//                            $message->from('NoReply@clientexchange.net', 'ClientExchange')->subject('Client Exchange Activation');
//                            $message->to($data['m_user_email_id'], $data['m_user_name']);
//                        });
                }else{
                    $update_data = array(
                        'm_compny_name' => $company_name,
                        'm_team_size' => $team_size,
                        //'m_user_email_id' => $m_user_email_id,
                        //'password' => $m_user_password
                    );
                    $data_id = array(
                        'm_compny_id' => $param,
                    );
                    $last_id = MembersCompny::putMemberCompny($data_id,$update_data);
                }
                
            }
            $responce = 'success';
            return Redirect::back()->withFlashMessage($responce);
            
        } catch (Exception $ex) {
            $type = 'postMemberCompny';
            App::make('HomeController')->postErrorLog($ex, $type);
        }
    }
    
    public function getUserAccountForm() {
        $user = '';
        // get service location
        $service_location = MemberUser::getServiceLocation($user);
        
        $data = array(
            'leftmenu' => 'useraccount',
            'action' => 'add',
            'service_location' => $service_location
        );
        return View::make('backend.memberscompny.useraccount',array('param' => $data));
    }
    
    public function postMemServiceLocation() {
        $mem_address = Input::get('mem_address');
        $mem_city = Input::get('mem_city');
        $mem_state = Input::get('mem_state');
        $mem_zip_code = Input::get('mem_zip_code');
        $mem_country = Input::get('mem_country');

        $data = array(
            'mem_address' => $mem_address,
            'mem_city' => $mem_city,
            'mem_state' => $mem_state,
            'mem_zip_code' => $mem_zip_code,
            'mem_country' => $mem_country,
            'time' => time()
        );
        MembersCompny::memberUserServiceLocation($data);
    }

}
