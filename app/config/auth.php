<?php

return array(

    'multi' => array(
        'account' => array(
            'driver' => 'eloquent',
            'model' => 'Account'
        ),
        'user' => array(
            'driver' => 'database',
            'table' => 'users'
        ),
        'admin' => array(
            'driver' => 'eloquent',
            'model' => 'Admin'
        ),
        'member_users' => array(
            'driver' => 'eloquent',
            'model' => 'MemberUser'
        ),
    ),

    'reminder' => array(

        'email' => 'emails.auth.reminder',

        'table' => 'password_reminders',

        'expire' => 60,

    ),

);